import React from 'react';
import Head from 'next/head';
import Modal from '../components/Modal/Modal';
import '../scss/main.scss';

const Register = () => (
  <div>
    <Head>
      <title>Puzzle | Register</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>
    <Modal />
  </div>
);

export default Register;
