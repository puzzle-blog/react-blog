import React from 'react';
import Navbar from '../components/Navbar/Navbar';
import Footer from '../components/Footer/Footer';
import Head from 'next/head';
import dynamic from 'next/dynamic';
import Page from '../components/PageWrapper/PageWrapper';
import '../scss/main.scss';

const DynamicComponentWithNoSSR = dynamic(
  () => import('../components/TextEditor/TextEditor'),
  { ssr: false }
);

const Write = () => {
  return (
    <>
      <Head>
        <title>Puzzle | Write</title>
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="stylesheet"
          href="//cdn.quilljs.com/1.2.6/quill.bubble.css"
        />
      </Head>
      <Page>
        <Navbar />
        <div className="editor">
          <DynamicComponentWithNoSSR />
        </div>
        {/* <Footer /> */}
      </Page>
    </>
  );
};

export default Write;
