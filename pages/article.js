import React from 'react';
import Navbar from '../components/Navbar/Navbar';
import Footer from '../components/Footer/Footer';
import Head from 'next/head';
import fetch from 'isomorphic-unfetch';
import Page from '../components/PageWrapper/PageWrapper';
import '../scss/main.scss';

const Article = ({ post: { title, desc, readTime, date, color, content } }) => {
  return (
    <>
      <Head>
        <title>Puzzle | Article</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Page>
        <div className="post-page">
          <Navbar />
          <div className="post">
            <h2>{title}</h2>
            <div className="time">{`${date} · ${readTime} min read`}</div>
            <div dangerouslySetInnerHTML={{ __html: content }} />
          </div>
        </div>
        <Footer />
      </Page>
    </>
  );
};

Article.getInitialProps = async context => {
  const res = await fetch(
    `https://blog-auth-backend.herokuapp.com/post/${context.query.id}`
  );
  const data = await res.json();

  return {
    post: data[0]
  };
};

export default Article;
