import React from 'react';
import Head from 'next/head';
import '../scss/main.scss';
import Navbar from '../components/Navbar/Navbar';
import Footer from '../components/Footer/Footer';
import Post from '../components/Post/Post';
import fetch from 'isomorphic-unfetch';
import Page from '../components/PageWrapper/PageWrapper';

const Articles = ({ posts }) => {
  return (
    <>
      <Head>
        <title>Puzzle | Posts</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Page>
        <Navbar />
        <div className="post-holder">
          {posts.map(post => {
            return <Post key={post._id} post={post} />;
          })}
        </div>
        <Footer />
      </Page>
    </>
  );
};

Articles.getInitialProps = async () => {
  const res = await fetch('https://blog-auth-backend.herokuapp.com/post');
  const data = await res.json();

  return {
    posts: data.reverse()
  };
};

export default Articles;
