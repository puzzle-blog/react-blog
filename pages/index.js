import React from 'react';
import Head from 'next/head';
import Navbar from '../components/Navbar/Navbar';
import Hero from '../components/Hero/Hero';
import '../scss/main.scss';

const Home = () => (
  <>
    <Head>
      <title>Puzzle | Home</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>
    <div className="index">
      <Navbar />
      <Hero />
    </div>
  </>
);

export default Home;
