import React from 'react';

const Hero = () => (
  <div className="hero">
    <div className="content">
      <h1 className="title">
        We believe in feeding minds, not mindless feeds🌿
      </h1>
      <p className="description">
        Become a Puzzle member for $5/month or $50/year and get unlimited access
        to the smartest writers and biggest ideas you won’t find anywhere else.
      </p>
      <a href="/articles">
        <button>Get started</button>
      </a>
    </div>
    <div className="background"></div>
  </div>
);

export default Hero;
