import React from 'react';
import Link from 'next/link';

const Modal = () => (
  <div className="modal-container">
    <div className="modal">
      <Link href="/">
        <img src="/assets/logo_transparent.png" />
      </Link>
      <hr />
      <h4>To continue, log in to Puzzle.</h4>
      <form className="modal-form">
        <input type="email" placeholder="Email" />
        <input type="password" placeholder="Password" />
        <input type="submit" value="Sign In" className="submit" />
      </form>
      <hr />
      <h4>Don't have an account?</h4>
      <button className="button-sign">Sign up for Puzzle</button>
    </div>
  </div>
);

export default Modal;
