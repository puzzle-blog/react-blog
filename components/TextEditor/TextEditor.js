import React, { useState } from 'react';
import ReactQuill from 'react-quill';
import { useRouter } from 'next/router';
import moment from 'moment';

const TextEditor = () => {
  const [content, setContent] = useState('');
  const [title, setTitle] = useState('');
  const [author, setAuthor] = useState('');
  const [pictureUrl, setPictureUrl] = useState(
    'https://designshack.net/wp-content/uploads/placeholder-image.png'
  );
  const [description, setDescription] = useState('');
  const readTime = Math.floor(content.split('').length / 1000);

  const router = useRouter();

  const submitHandler = () => {
    const newPostData = {
      title,
      content,
      pictureUrl,
      author,
      description,
      readTime
    };

    console.log(newPostData);

    fetch('https://blog-auth-backend.herokuapp.com/post', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newPostData)
    })
      .then(response => console.log(response))
      .then(data => console.log(data))
      .catch(error => console.log(error));

    router.push('/articles');
  };

  return (
    <div className="te-container">
      <div className="te-meta">
        <input
          type="text"
          value={author}
          onChange={e => setAuthor(e.target.value)}
          placeholder="Name"
          maxlength="25"
        />
        <input type="text" value={`${readTime}  min read☕`} />
        <input type="text" value={moment().format('MMM, DD  YYYY')} />
      </div>
      <input
        className="title"
        type="text"
        value={title}
        onChange={e => setTitle(e.target.value)}
        placeholder="Fancy title goes here..."
        maxlength="54"
      />
      <ReactQuill
        theme="bubble"
        className="r-quill"
        value={content}
        onChange={setContent}
      />

      <div className="post-editor">
        <input
          type="text"
          value={description}
          onChange={e => setDescription(e.target.value)}
          className="description"
          placeholder="Short description"
        />
        <input
          type="text"
          value={pictureUrl}
          onChange={e => setPictureUrl(e.target.value)}
          className="picture-url"
        />
      </div>

      <div className="submit-button" onClick={submitHandler}>
        Submit article 🚀
      </div>
    </div>
  );
};

export default TextEditor;
