import React from 'react';
import Link from 'next/link';
import moment from 'moment';

const Post = ({
  post: { title, description, readTime, date, color, _id, author, pictureUrl }
}) => {
  return (
    <Link href={`/article?id=${_id}`}>
      <div className="post-cover">
        <div className="content">
          <h3 className="title">{title}</h3>
          <p className="description">{description}</p>
          <div className="post-meta">
            <div className="date">{`${moment(date).format(
              'MMM DD, YYYY'
            )} · ${readTime} min read`}</div>
            <div className="author">{author}</div>
          </div>
        </div>
        <img style={{ backgroundColor: color }} src={pictureUrl} />
      </div>
    </Link>
  );
};

export default Post;
