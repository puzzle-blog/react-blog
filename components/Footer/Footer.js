import React from 'react';
import Link from 'next/link';

const Footer = () => (
  <div className="footer">
    <div>© 2019 Puzzle | Made with 💚 by Alek Karp</div>
  </div>
);

export default Footer;
