import React from 'react';
import Link from 'next/link';

const links = [
  { href: '/write', label: 'Write' },
  { href: '/articles', label: 'Articles' },
  {
    href: '/register',
    label: 'Sign In',
    className: 'sign'
  },
  {
    href: '/register',
    label: 'Register Now',
    className: 'register'
  }
].map(link => {
  link.key = `nav-link-${link.href}-${link.label}`;
  return link;
});

const Nav = () => (
  <nav className="navbar">
    <Link href="/">
      <img className="logo" src="/assets/logo_transparent.png" />
    </Link>
    <ul className="links-group">
      {links.map(({ key, href, label, className }) => (
        <li key={key} className={className}>
          <a href={href}>{label}</a>
        </li>
      ))}
    </ul>
  </nav>
);

export default Nav;
